

<!DOCTYPE html>
<html>
    <head>
        <title>Confirmation</title>
        <link  href="https://getbootstrap.com/docs/4.3/examples/checkout/">

            <!-- Bootstrap core CSS -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    </head>
    <body>
        <?php
        echo '    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>';
        echo ' <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>';
        echo '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>';
        echo "<h1> SUMMARY </h1> ";
        echo '<table class="table table-striped">';
        echo '<tbody>';
        echo "<tr>";
            echo '<th scope="row"> First Name : </th>';
            echo "<td>".$_POST['firstName']."</td>";
        echo "</tr>";
        echo "<tr>";
            echo '<th scope="row"> Last Name : </th>';
            echo '<td>'.$_POST['lastName']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Email : </th>';
            echo '<td>'.$_POST['email']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Shipping Address : </th>';
            echo '<td>'.$_POST['address']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Apt/Suite : </th>';
            echo '<td>'.$_POST['apt']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Country : </th>';
            echo '<td>'.$_POST['country']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Province/Territory : </th>';
            echo '<td>'.$_POST['province']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Postal Code : </th>';
            echo '<td>'.$_POST['pCode']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Gift Wrap : </th>';
            echo '<td>'.$_POST['giftWrap']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Delivery Option : </th>';
            echo '<td>'.$_POST['Delivery']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Name on card : </th>';
            echo '<td>'.$_POST['cardName']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Card Number : </th>';
            echo '<td>'.$_POST['cardNo']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Expiration Date : </th>';
            echo '<td>'.$_POST['expiration']."</td>";
        echo "</tr>";
        echo "<tr>";    
            echo '<th scope="row"> Security Code : </th>';
            echo '<td>'.$_POST['secCode'].'</td>';
        echo "</tr>";
         echo '<tbody>'; 
        
        $qty1 = $_POST['qty1'];
        $qty2 = $_POST['qty2'];
        $qty3 = $_POST['qty3'];
        $giftWrap = $_POST['giftWrap'];
        $mail = $_POST['Delivery'];
        $province = $_POST['province'];
        
        $deliverRate;
        $tax = array( 0.05 , 0.11 , 0.12 , 0.13 , 0.14975 , 0.15 );
        $provinceTax;
        $grandVal;
        
        if($mail == "mail"){
            $deliverRate = 7.65;
        }
        elseif($mail == "courier"){
            $deliverRate = 11.25;
        }
        else{
            $deliverRate = 4.55;
        }
        
        $totalVal = ((12 * $qty1) + (8 * $qty2) + (5 * $qty3)+$deliverRate) ;
        
        
        if (strcmp($province, ("AB" || "NT" || "NU" || "YT")) == 0){
            $provinceTax = $tax[0];
        }
        elseif (strcmp($province, "SK") == 0){
             $provinceTax = $tax[1];
        }
        elseif (strcmp($province, "BC") == 0){
             $provinceTax = $tax[2];
        }
        elseif (strcmp($province, ("MB" || "ON" )) == 0){
             $provinceTax = $tax[3];
        }
        elseif (strcmp($province, "QC") == 0){
             $provinceTax = $tax[4];
        }
        else {
             $provinceTax = $tax[5];
        }
        
        
        if($giftWrap == true){
            $grandVal = ((($totalVal + 3.49) * $provinceTax) + ($totalVal + 3.49) );
            echo " <b>Total Value : </b>".( $totalVal + 3.49 ) ."<br>";
            echo "<b>Grand Total :</b>". $grandVal  ."<br>" ;
        }
        else{
            $grandVal = ($totalVal + ($totalVal * ($provinceTax)));
            echo "<b>Total Value :</b>".$totalVal ."<br>" ;
            echo "<b>Grand Total :</b>". $grandVal ."<br>" ;
        }
        
        if( $grandVal > 750 ){
            echo "<script type='text/javascript'>alert('The credit card has been declined!')</script>";
        }
        ?>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>