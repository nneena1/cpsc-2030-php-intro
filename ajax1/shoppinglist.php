<?php
    header( "Content-type: application/json");
    
    $link = mysqli_connect( 'localhost', 'root', '19981125--N' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    $dbName = "demo";
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    switch ( $_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $results = mysqli_query( $link, 'select * from cars' );
            
            if ( ! $results ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $cars = array();
                while( $record = mysqli_fetch_assoc( $results ) ) {
                    $cars[] = $record;
                }
                mysqli_free_result( $results );
                echo json_encode( $cars);
            }
            break;
        
        case 'POST':
            $safe_make= mysqli_real_escape_string( $link, $_REQUEST["Make"] );
            $safe_model= mysqli_real_escape_string( $link, $_REQUEST["Model"] );
            $year = $_REQUEST["Year"] + 0;
             $mileage = $_REQUEST["Mileage"] + 0;
            if ( strlen( $safe_make ) <= 0 ||
                 strlen( $safe_make ) > 80 ||
                 strlen( $safe_model ) <= 0 ||
                 strlen( $safe_model ) > 80 ||
                 $year <= 0||
                 $mileage <= 0 ) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                exit(1);
            }

            $query = "INSERT INTO cars ( Make, Model , Year, Mileage ) VALUES ( '$safe_make', '$safe_model', $year , $mileage )";
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            break;
        
        case 'DELETE':
            $ID = $_REQUEST['ID'] + 0;
             console.log("$ID") ;
            $query1 = "DELETE FROM cars WHERE ID = '$ID'";
            
            if ( ! mysqli_query( $link, $query1 ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
            }
            
            break;

    }
