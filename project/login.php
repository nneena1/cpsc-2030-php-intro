<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


		<link rel="stylesheet" href="custom.css">
		
		<title>Project CPSC-2030</title>
	</head>
	
	<body id="login">
		 <?php session_start(); ?>
		 <form method="POST" action="">
		<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="index.php" style="color:white">Royal Library</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
			  <li class="nav-item active">
				<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="search.php">Search</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="hours.php">Library Hours</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="login.php" class="text-right">Login</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="documentation.html" >Documentation & Sources</a>
			  </li>
			</ul>
		  </div>
		</nav>
		//login Modal
		<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		  aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header text-center">
				<h4 class="modal-title w-100 font-weight-bold" style="color:black;" > Sign in</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body mx-3">
				<div class="md-form mb-2">
				  <i class="fas fa-envelope prefix grey-text"></i>
				  <input name="email" type="email" id="defaultForm-email" class="form-control validate">
				  <label data-error="wrong" data-success="right" for="defaultForm-email">Your email</label>
				</div>

				<div class="md-form mb-2">
				  <i class="fas fa-lock prefix grey-text"></i>
				  <input name="password" type="password" id="defaultForm-pass" class="form-control validate">
				  <label data-error="wrong" data-success="right" for="defaultForm-pass">Your password</label>
				</div>
			</div>
			  <div class="modal-footer d-flex justify-content-center">
				<button id="login" class="btn btn-info" >Login</button>
			  </div>
			</div>
		  </div>
		</div>

		<div class="text-center">
		  <a href="" class="btn btn-secondary btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm"> Click here to Login </a>
		</div>
		 </form>			
	  <?php
	       $link = mysqli_connect( 'localhost', 'root', '19981125--N' );
	       if (!$link) //error for no connection
	        {
	        echo('Could not connect: ' . mysql_error());
	        }
	        
	        mysqli_select_db( $link, 'demo' );
	        if (isset($_POST['email']) and isset($_POST['password'])){
	        	
	        // Assigning POST values to variables.
	        $username = $_POST['email'];
	        $password = $_POST['password'];
	        
	        $query = "SELECT * FROM `Login` WHERE Email='$username' and Password='$password'";
	         
	        $result = mysqli_query($link, $query) or die(mysqli_error($link));
	          if(!mysqli_num_rows($result)){
	             echo 'Invalid information';
	         }
	        $count = mysqli_num_rows($result);
	        session_start();
	        if ($count == 1){
	          //username is neena00 and password is neena00
	          $_SESSION['loggedin'] = true;
	          header('Location: insert.php');
	       }else{
	          $_SESSION['loggedin'] = false;
	        }
	        }
	        
	        if(!mysqli_close($link)){
	           echo 'Unable to close';
	       }
	       mysqli_close($link);
	       
	  ?>				
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	</header>
	</body>
</html>
