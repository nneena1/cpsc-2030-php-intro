<?php
    session_start();
    if ( ! isset ( $_SESSION["loggedin"] ) ) {
      $_SESSION["loggedin"] = false;
    }
    $loggedin = $_SESSION['loggedin'];
?>

<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


		<link rel="stylesheet" href="custom.css">
		
		<title>Project CPSC-2030</title>
	</head>
	
	<body id="insert">
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="index.php" style="color:white">Royal Library</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
			  <li class="nav-item active">
				<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="search.php">Search</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="hours.php">Library Hours</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="documentation.html" >Documentation & Sources</a>
			  </li>
      </ul>
      <div class="col-3 text-right">
          <?php if ( $loggedin ) { ?>
            <a class="btn btn-primary align-middle" href="logout.php" role="button">Logout</a>
          <?php } else { ?>
            <a class="btn btn-primary align-middle" href="login.php" role="button">Login</a>
          <?php } ?>        
        </div>
		  </div>
		</nav>
		
    <form method="POST" action="">
      <div class="form-row" id="exampleModal">
        <div class="form-group col-md-6">
          <label for="fname">Author's First Name</label>
          <input name="fname" type="text" class="form-control" id="fname" placeholder="First Name" required >
          <div class="invalid-feedback">
            Valid First Name is required.
          </div>
        </div>
        <div class="form-group col-md-6">
          <label for="lname">Author's Last Name</label>
          <input name="lname" type="text" class="form-control" id="lname" placeholder="Last Name" required>
          <div class="invalid-feedback">
            Valid Last Name is required.
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="book">Book Title</label>
        <input name="book" type="text" class="form-control" id="book" placeholder="Abc Def" required>
          <div class="invalid-feedback">
            Valid Book title is required.
          </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="edition">Edition</label>
          <input name="edition" type="number" class="form-control" id="edition" placeholder="1" required>
          <div class="invalid-feedback">
            Valid edition is required.
          </div>
        </div>
        <div class="form-group col-md-4">
          <label for="year">Year</label>
          <input name="year" type="number" class="form-control" id="year" placeholder="20XX" required>
          <div class="invalid-feedback">
            Valid year is required.
          </div>
        </div>
        <div class="form-group col-md-4">
          <label for="location">Location</label>
          <input name="location" type="text" class="form-control" id="location" placeholder="2E" required>
          <div class="invalid-feedback">
            Valid location is required.
          </div>
        </div>
      </div>
      <button name="submit" type="submit" class="btn btn-primary">Add this book</button><br>
      </form>
      <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Author's First Name </th>
                <th scope="col">Author's Last Name</th>
                <th scope="col">Book</th>
                <th scope="col">Edition</th>
                <th scope="col">Year</th>
                <th scope="col">Location</th>
                <th scope="col">Delete</th> 
            </tr>
        </thead>
        <tbody>
            <?php
            $link = mysqli_connect( 'localhost', 'root', '19981125--N' );
            if ( ! $link ) {
              $error_number = mysqli_connect_errno();
              $error_message = mysqli_connect_error();
              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
              exit(1);
            }
            $dbName = "demo";
            if ( ! mysqli_select_db( $link, $dbName ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
              exit(1);
            }
            
            if ( isset( $_REQUEST['id'] ) ) {
              $ID = $_REQUEST['id'] + 0;
              $query = "delete from books where ID = $ID";

              if ( ! mysqli_query( $link, $query ) ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
              }
            } else if ( isset( $_REQUEST["fname"] ) ) {
              //error messages
                $safe_fname = mysqli_real_escape_string( $link, $_REQUEST["fname"] );
                $safe_lname = mysqli_real_escape_string( $link, $_REQUEST["lname"] );
                $safe_book = mysqli_real_escape_string( $link, $_REQUEST["book"] );
                $edition = $_REQUEST["edition"] + 0;
                $year = $_REQUEST["year"] + 0;
                $location = $_REQUEST["location"] + 0;
                $error_message = "";
                if ( strlen( $safe_fname ) <= 0 ) {
                    $error_message .= "invalid first name: $safe_fname, ";
                }
                if ( strlen( $safe_lname ) <= 0 ) {
                    $error_message .= "invalid last name: $safe_lname, ";
                }
                if ( strlen( $safe_book ) <= 0 ) {
                    $error_message .= "invalid book name: $safe_book, ";
                }
                if ( $edition <= 0 ) {
                    $error_message .= "invalid edition: $edition, ";
                }
                if ( $year <= 0 ) {
                    $error_message .= "invalid year: $year, ";
                }
                if ( $location <= 0 ) {
                    $error_message .= "invalid location: $location";
                }
                
                if ( strlen( $error_message ) > 0 ) {
                    print "<tr><td colspan='6' class='alert alert-danger'>$error_message</td></tr>";   
                } else {
                    $query = "INSERT INTO books (authorFname ,authorLname,book,edition,year,location ) VALUES ('$safe_fname', '$safe_lname','$safe_book', $edition, $year ,'$location')";
                  
                    if ( ! mysqli_query( $link, $query ) ) {
                      $error_number = mysqli_errno( $link );
                      $error_message = mysqli_error( $link );
                      print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
                   }
                    echo '<script language="javascript">';
                    echo 'alert("added successfully")';
                    echo '</script>';
                }
            }
            
            $results = mysqli_query( $link, 'select * from books' );
            
            if ( ! $results ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";   
            } else {
              while( $record = mysqli_fetch_assoc( $results ) ) {
                $ID = $record['ID'];
                $authorFname = $record['authorFname'];
                $authorLname = $record['authorLname'];
                $book = $record['book'];
                $year = $record['year'];
                $edition = $record['edition'];
                $location = $record['location'];
                $delete = $loggedin ? "<form action='' method='post' id='del' ><input type='hidden' name='id' value='$ID'><button class='btn btn-danger'  type='submit' >Delete</button></form>" : "";
                print "<tr><td>$ID</td><td>$authorFname</td><td>$authorLname</td><td>$book</td><td>$year</td><td>$edition</td><td>$location</td> <td colspan='1'>$delete</td></tr>";
               // to represent them in the table 
              }
              mysqli_free_result( $results );
            }
            if ( ! mysqli_close( $link ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              print "<tr><td colspan='6' class='alert alert-danger'>($error_number) $error_message</td></tr>";  
              //message if not connected to database
            }
            ?>
        </tbody>
    </table>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	</header>
	</body>
</html>