<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		
		<link rel="stylesheet" href="custom.css">
		
		<title>Project CPSC-2030</title>
	</head>
	
	<body id="search">
		<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="index.php" style="color:white">Royal Library</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
			  <li class="nav-item active">
				<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="search.php">Search</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="hours.php">Library Hours</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="login.php">Login</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="documentation.html" >Documentation & Sources</a>
			  </li>
			</ul>
		  </div>
		</nav>

		  
			<form method="POST" action="">
			  <div class="form-row">
				
				<div class="form-group col-md-7">
				<br>
				  <label for="firstName">Author's First Name</label>
				  <input name="firstName" type="text" class="form-control" id="firstName" placeholder="First Name">
				</div>
				<div class="form-group col-md-7">
				  <label for="lastName">Author's Last Name</label>
				  <input name="lastName" type="text" class="form-control" id="lastName" placeholder="Last Name">
				</div>
				<div class="form-group col-md-6">
				  <label for="book">Book Title</label>
				  <input name="book" type="text" class="form-control" id="book" placeholder="Book">
				</div>
			  </div>
			  <button type="submit" class="btn btn-dark">Search</button>
			</form>

	   <table class="table">
	        <thead>
	            <tr>
	                <th scope="col">ID</th>
	                <th scope="col">Book</th>
	                <th scope="col">Edition</th>
	                <th scope="col">Year</th>
	                <th scope="col">Location</th>
	            </tr>
	        </thead>
	        <tbody>
	            <?php
	            	 $link = mysqli_connect( 'localhost', 'root', '19981125--N' );
	                mysqli_select_db( $link, 'demo' );
	                $fName = $_POST['firstName'];
	        		$lName = $_POST['lastName'];
	        		$bookName = $_POST['book'];
	        		
	                $results = mysqli_query( $link, "SELECT * FROM books WHERE authorFname='$fName' and authorLname='$lName' and book='$bookName'" );
	                // process $results
	        
	               while( $record = mysqli_fetch_assoc( $results ) ) {
	                	$ID = $record['ID'];
	                	$bookName = $record['book'];
	        			$edition = $record['edition'];
	        			$year = $record['year'];
	        			$location = $record['location'];
	                	print "<tr><td>$ID</td><td>$bookName</td><td>$edition</td><td> $year</td><td> $location</td></tr>";
	                }
	               ?>
	        </tbody>
	    </table>
		
		</header>
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		

	</body>
</html>