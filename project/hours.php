<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		
		<link rel="stylesheet" href="custom.css">
		
		<title>Project CPSC-2030</title>
	</head>
	
	<body id="hours">
		<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="index.php" style="color:white">Royal Library</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
			  <li class="nav-item active">
				<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="search.php">Search</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="hours.php">Library Hours</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="login.php" >Login</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="documentation.html" >Documentation & Sources</a>
			  </li>
			</ul>
		  </div>
		</nav>

		<div class="container">
			<h1 class="text-center">Library Hours</h1>
			<div class="grid-container">
			  <div class="grid-item">
				<h2>Fall/Winter Hours</h2>
				<p><span>Mon - Fri:</span> 7:30 a.m. - 10:00 p.m.<br><span>Saturday:</span> 7:30 a.m. - 5:00 p.m.<br><span>Sunday:</span> Closed</p>
			  </div>
			  <div class="grid-item">
				<h2>Summer Hours</h2>
				<p><span>Mon - Thurs:</span> 8:00 a.m. - 7:00 p.m.<br><span>Friday:</span> 8:00 a.m. - 5:00 p.m.<br><span>Sat & Sun:</span> Closed
				<br>*No entry permitted 15 mins. before closing</p>
			  </div>
			</div>	
		</div>

		</header>
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		

	</body>
</html>