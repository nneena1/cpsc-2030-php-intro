<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <h1>Cars - CPSC 2030</h1>
    <title>Cars - CPSC 2030</title>
   <div class="text-left">
    <button type="button" class="btn btn-primary btn-md " onclick="window.location.href = 'login.php';">Login</button>
    </div>
  </head>
  <body class="container">
    
    <?php session_start(); ?>
    <?php if  ( $_SESSION["loggedin"]) { ?>
     <div class="text-right">
    <button type="button" class="btn btn-primary btn-md " onclick="window.location.href = 'logout.php';">Logout</button>
    </div>
    
    <form class="needs-validation" novalidate method="POST" action="index.php">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="make">Make</label>
            <input type="text" class="form-control" id="make" placeholder="" value="" required name="make">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder="" value="" required name="model">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="year">Year</label>
            <input type="text" class="form-control" id="year" placeholder="" value="" required name="year">
            <div class="invalid-feedback">
              Valid year is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="mileage">Mileage</label>
            <input type="text" class="form-control" id="mileage" placeholder="" value="" required name="mileage">
            <div class="invalid-feedback">
              Valid mileage is required.
            </div>
          </div>
         <button name="submit" class="btn btn-primary btn-lg btn-block" type="submit">Add car</button>
          
        <form class="needs-validation" novalidate method="POST" action="">
              <input type="hidden" name="Delete">
          </form>
        </div>
       </form>
       <?php } ?>
    
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Make</th>
                <th scope="col">Model</th>
                <th scope="col">Year</th>
                <th scope="col">Mileage</th>
            </tr>
        </thead>
        <tbody>
        <?php
           session_start();
           $link = mysqli_connect( 'localhost', 'root', '19981125--N' );
           if (!$link) //error for no connection
            {
            die('Could not connect: ' . mysql_error());
            }
            //connect to database
            if(!mysqli_select_db( $link, 'demo' )){
                echo 'Database not selected';
            }
            
            if(isset($_POST['submit'])){
              if(is_numeric(trim($_POST["year"])) == false || trim($_POST["year"]) == ""){
                echo '<script language="javascript"> alert("Invalid Year")</script>';
              }
              elseif(is_numeric(trim($_POST["mileage"])) == false || trim($_POST["mileage"]) == ""){
               echo '<script language="javascript"> alert("Invalid Mileage")</script>';
              }
              elseif(is_numeric(trim($_POST["make"])) == true || trim($_POST["make"]) == ""){
                echo '<script language="javascript"> alert("Invalid Make")</script>';
              }
              elseif(is_numeric(trim($_POST["model"])) == true || trim($_POST["model"]) == ""){
                echo '<script language="javascript"> alert("Invalid Model")</script>';
              }
              
              else{
              $postmake = $_POST['make'];
              $postmodel = $_POST['model'];
              $postyear = $_POST['year'];
              $postmileage = $_POST['mileage'];
            }
    
            }
            
            $sql="INSERT INTO cars (Make,Model,Year,Mileage) VALUES ('$postmake','$postmodel','$postyear','$postmileage')";
             mysqli_query( $link, $sql );
            
            
            /// delete data from database
            $query= "DELETE FROM 'cars' WHERE ID='$id'";
            
            
           //retrieving data from database
            $results = mysqli_query( $link, 'SELECT * FROM cars' );
            
            if(! mysqli_fetch_assoc( $results ) ){
                echo 'Retrieve Query not selected';
            }
            while( $record = mysqli_fetch_assoc( $results ) ) {
              $ID = $record['ID'];
            	$make = $record['Make'];
            	$model = $record['Model'];
            	$year = $record['Year'];
            	$mileage = $record['Mileage'];
            	print "<tr><td>$ID</td><td>$make</td><td> $model</td><td> $year</td><td> $mileage</td> <td><button>Delete</button></td> </tr>";
            }
           ?>
          
        </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
   
  </body>
</html>